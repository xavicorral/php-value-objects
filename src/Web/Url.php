<?php

namespace ValueObjects\Web;

use ValueObjects\AbstractValueObject;
use ValueObjects\Exception\Web\InvalidUrlException;

class Url extends AbstractValueObject
{
    protected function guard($value)
    {
        if (false === filter_var($value, FILTER_VALIDATE_URL)) {
            throw new InvalidUrlException($value);
        }

        return true;
    }
}