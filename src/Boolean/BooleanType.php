<?php

declare(strict_types=1);

namespace ValueObjects\Boolean;

use ValueObjects\AbstractValueObject;
use ValueObjects\Exception\Boolean\InvalidBooleanTypeException;

class BooleanType extends AbstractValueObject
{
    protected function guard($value)
    {
        if (!is_bool($value)) {
            throw new InvalidBooleanTypeException($value);
        }

        return true;
    }

    public static function fromString($value): self
    {
        return new self(filter_var($value, FILTER_VALIDATE_BOOLEAN));
    }

    public static function true()
    {
        return new self(true);
    }

    public static function false()
    {
        return new self(false);
    }

    public function isTrue(): bool
    {
        return true === $this->value;
    }

    public function isFalse(): bool
    {
        return false === $this->value;
    }

    public function toInt(): int
    {
        return $this->value? 1 : 0;
    }

    public function toString(): string
    {
        return $this->value? 'true' : 'false';
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}
