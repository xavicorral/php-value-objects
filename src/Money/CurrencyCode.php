<?php

namespace ValueObjects\Money;

use Exception;
use Symfony\Component\Intl\Currencies;
use ValueObjects\AbstractValueObject;
use ValueObjects\Exception\Money\InvalidCurrencyCodeException;

class CurrencyCode extends AbstractValueObject
{
    /**
     * Guard that value object is valid.
     *
     * @param string $value
     *
     * @return boolean
     * @throws InvalidCurrencyCodeException
     */
    protected function guard($value)
    {
        $value = $this->normalizeValue($value);
        try {
            Currencies::getName($value);
        } catch(Exception $e) {
            throw new InvalidCurrencyCodeException($value);
        }

        return true;
    }

    protected function normalizeValue($value): string
    {
        return strtoupper($value);
    }

    public function symbol(): string
    {
        return Currencies::getSymbol($this->value);
    }
}