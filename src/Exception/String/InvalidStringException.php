<?php

namespace ValueObjects\Exception\String;

final class InvalidStringException extends \InvalidArgumentException
{
    /**
     * @param mixed $value
     */
    public function __construct($value)
    {
        parent::__construct(sprintf('Invalid string value <%s>', $value));

        $this->code = 'invalid_string';
    }
}