<?php

namespace ValueObjects\Exception\Time;

final class InvalidMonthException extends \InvalidArgumentException
{
    /**
     * @param mixed $value
     */
    public function __construct($value)
    {
        parent::__construct(sprintf('Invalid month value <%s>', $value));

        $this->code = 'invalid_month';
    }
}