<?php

namespace ValueObjects\Exception\Time;

final class InvalidDateTimeException extends \InvalidArgumentException
{
    /**
     * @param mixed $value
     */
    public function __construct($value, string $format)
    {
        parent::__construct(sprintf('Invalid datetime value <%s> with format <%s>', $value, $format));

        $this->code = 'invalid_datetime';
    }
}