<?php

namespace ValueObjects\Exception\Time;

final class InvalidMinuteException extends \InvalidArgumentException
{
    /**
     * @param mixed $value
     */
    public function __construct($value)
    {
        parent::__construct(sprintf('Invalid minute value <%s>', $value));

        $this->code = 'invalid_minute';
    }
}