<?php

namespace ValueObjects\Exception\Time;

final class InvalidHourException extends \InvalidArgumentException
{
    /**
     * @param mixed $value
     */
    public function __construct($value)
    {
        parent::__construct(sprintf('Invalid hour value <%s>', $value));

        $this->code = 'invalid_hour';
    }
}