<?php

namespace ValueObjects\Exception\Time;

final class InvalidTimeException extends \InvalidArgumentException
{
    /**
     * @param mixed $value
     */
    public function __construct($value)
    {
        parent::__construct(sprintf('Invalid time value <%s>', $value));

        $this->code = 'invalid_time';
    }
}