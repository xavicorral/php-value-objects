<?php

namespace ValueObjects\Exception\Identity;

/**
 * Class InvalidAsinException.
 */
final class InvalidAsinException extends \InvalidArgumentException
{
    /**
     * @param mixed $value
     */
    public function __construct($value)
    {
        parent::__construct(sprintf('Invalid ASIN value <%s>', $value));

        $this->code = 'invalid_asin';
    }
}