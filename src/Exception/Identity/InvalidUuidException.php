<?php

namespace ValueObjects\Exception\Identity;

/**
 * Class InvalidUuidException.
 */
final class InvalidUuidException extends \InvalidArgumentException
{
    /**
     * @param mixed $value
     */
    public function __construct($value)
    {
        parent::__construct(sprintf('Invalid UUID value <%s>', $value));

        $this->code = 'invalid_uuid';
    }
}