<?php

declare(strict_types=1);

namespace ValueObjects\Exception\Bank;

final class InvalidSwiftCodeException extends \InvalidArgumentException
{
    public function __construct($value)
    {
        parent::__construct(sprintf('Invalid swift code value <%s>', $value));

        $this->code = 'invalid_swift_code';
    }
}
