<?php

declare(strict_types=1);

namespace ValueObjects\Exception\Bank;

final class InvalidIbanException extends \InvalidArgumentException
{
    public function __construct($value)
    {
        parent::__construct(sprintf('Invalid IBAN value <%s>', $value));

        $this->code = 'invalid_iban';
    }
}
