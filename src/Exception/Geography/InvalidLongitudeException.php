<?php

namespace ValueObjects\Exception\Geography;

/**
 * Class InvalidLongitudeException.
 */
final class InvalidLongitudeException extends \InvalidArgumentException
{
    /**
     * @param mixed $value
     */
    public function __construct($value)
    {
        parent::__construct(sprintf('Invalid longitude value <%s>', $value));

        $this->code = 'invalid_longitude';
    }
}