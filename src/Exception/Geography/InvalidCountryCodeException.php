<?php

namespace ValueObjects\Exception\Geography;

final class InvalidCountryCodeException extends \InvalidArgumentException
{
    public function __construct($value)
    {
        parent::__construct(sprintf('Invalid country code value <%s>', $value));

        $this->code = 'invalid_country_code';
    }
}