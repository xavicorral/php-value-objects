<?php

namespace ValueObjects\Exception\Geography;

final class InvalidLanguageCodeException extends \InvalidArgumentException
{
    public function __construct($value)
    {
        parent::__construct(sprintf('Invalid language code value <%s>', $value));

        $this->code = 'invalid_language_code';
    }
}