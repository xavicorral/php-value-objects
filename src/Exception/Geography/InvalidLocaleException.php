<?php

namespace ValueObjects\Exception\Geography;

final class InvalidLocaleException extends \InvalidArgumentException
{
    public function __construct($value)
    {
        parent::__construct(sprintf('Invalid locale value <%s>', $value));

        $this->code = 'invalid_locale';
    }
}