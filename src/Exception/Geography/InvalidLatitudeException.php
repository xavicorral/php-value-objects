<?php

namespace ValueObjects\Exception\Geography;

/**
 * Class InvalidLatitudeException.
 */
final class InvalidLatitudeException extends \InvalidArgumentException
{
    /**
     * @param mixed $value
     */
    public function __construct($value)
    {
        parent::__construct(sprintf('Invalid latitude value <%s>', $value));

        $this->code = 'invalid_latitude';
    }
}