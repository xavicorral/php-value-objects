<?php

declare(strict_types=1);

namespace ValueObjects\Exception\Boolean;

final class InvalidBooleanTypeException extends \InvalidArgumentException
{
    public function __construct($value)
    {
        parent::__construct(sprintf('Invalid boolean value <%s>', $value));

        $this->code = 'invalid_boolean';
    }
}