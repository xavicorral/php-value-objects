<?php

namespace ValueObjects\Exception\Web;

final class InvalidUrlException extends \InvalidArgumentException
{
    /**
     * @param mixed $value
     */
    public function __construct($value)
    {
        parent::__construct(sprintf('Invalid URL value <%s>', $value));

        $this->code = 'invalid_url';
    }
}