<?php

namespace ValueObjects\Exception\Web;

final class InvalidEmailAddressException extends \InvalidArgumentException
{
    /**
     * @param mixed $value
     */
    public function __construct($value)
    {
        parent::__construct(sprintf('Invalid Email address value <%s>', $value));

        $this->code = 'invalid_email_address';
    }
}