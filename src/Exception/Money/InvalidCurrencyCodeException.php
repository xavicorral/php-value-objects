<?php

namespace ValueObjects\Exception\Money;

/**
 * Class InvalidCurrencyCodeException.
 */
final class InvalidCurrencyCodeException extends \InvalidArgumentException
{
    /**
     * @param mixed $value
     */
    public function __construct($value)
    {
        parent::__construct(sprintf('Invalid currency code value <%s>', $value));

        $this->code = 'invalid_currency_code';
    }
}