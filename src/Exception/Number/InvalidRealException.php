<?php

namespace ValueObjects\Exception\Number;

/**
 * Class InvalidRealException.
 */
final class InvalidRealException extends \InvalidArgumentException
{
    /**
     * @param mixed $value
     */
    public function __construct($value)
    {
        parent::__construct(sprintf('Invalid real value <%s>', $value));

        $this->code = 'invalid_real';
    }
}