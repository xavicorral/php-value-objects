<?php

namespace ValueObjects\Exception\Number;

/**
 * Class InvalidNaturaException.
 */
final class InvalidNaturaException extends \InvalidArgumentException
{
    /**
     * @param mixed $value
     */
    public function __construct($value)
    {
        parent::__construct(sprintf('Invalid natural number value <%s>', $value));

        $this->code = 'invalid_natural';
    }
}