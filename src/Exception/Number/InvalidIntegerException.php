<?php

namespace ValueObjects\Exception\Number;

/**
 * Class InvalidIntegerException.
 */
final class InvalidIntegerException extends \InvalidArgumentException
{
    /**
     * @param mixed $value
     */
    public function __construct($value)
    {
        parent::__construct(sprintf('Invalid integer value <%s>', $value));

        $this->code = 'invalid_integer';
    }
}