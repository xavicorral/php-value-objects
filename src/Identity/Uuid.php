<?php

namespace ValueObjects\Identity;

use Ramsey\Uuid\Uuid as BaseUuid;
use ValueObjects\AbstractValueObject;
use ValueObjects\Exception\Identity\InvalidUuidException;

class Uuid extends AbstractValueObject
{
    /**
     * Guard that value object is valid.
     *
     * @param string $value
     *
     * @return boolean
     * @throws InvalidUuidException
     */
    protected function guard($value)
    {
        if (!BaseUuid::isValid($value)){
            throw new InvalidUuidException($value);
        }

        return true;
    }

    /**
     * Generate a new Uuid.
     */
    public static function generate()
    {
        return new Uuid(BaseUuid::uuid4());
    }
}