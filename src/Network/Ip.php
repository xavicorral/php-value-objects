<?php

namespace ValueObjects\Network;

use ValueObjects\AbstractValueObject;
use ValueObjects\Exception\Network\InvalidIpException;

class Ip extends AbstractValueObject
{
    const IPV4 = 'IPv4';
    const IPV6 = 'IPv6';

    /**
     * Guard that value object is valid.
     *
     * @param integer $value
     *
     * @return boolean
     * @throws InvalidIpException
     */
    protected function guard($value)
    {
        $filteredValue = filter_var($value, FILTER_VALIDATE_IP);
        if ($filteredValue === false) {
            throw new InvalidIpException($value);
        }

        return true;
    }

    public function getVersion(): string
    {
        if ($this->isIpV4()) {
            return self::IPV4;
        }

        return self::IPV6;
    }

    public function isIpV4(): bool
    {
        $isIPv4 = filter_var($this->value, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);

        return (false !== $isIPv4);
    }

    public function isIpV6(): bool
    {
        $isIPv6 = filter_var($this->value, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6);

        return (false !== $isIPv6);
    }
}