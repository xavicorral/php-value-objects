<?php

namespace ValueObjects\Geography;

use ValueObjects\AbstractValueObject;
use ValueObjects\Exception\Geography\InvalidLongitudeException;

class Longitude extends AbstractValueObject
{
    private const MIN_LONGITUDE = -180;
    private const MAX_LONGITUDE = 180;

    /**
     * Guard that value object is valid.
     *
     * @param mixed $value
     *
     * @return boolean
     * @throws InvalidLongitudeException
     */
    protected function guard($value)
    {
        $filteredValue = filter_var($value, FILTER_VALIDATE_FLOAT);

        // FILTER_VALIDATE_FLOAT validates true as 1.
        if (true === $value || false === $filteredValue || $value < self::MIN_LONGITUDE || $value > self::MAX_LONGITUDE){
            throw new InvalidLongitudeException($value);
        }

        return true;
    }

    /**
     * Convert the valid longitude (string, int...) to native float.
     *
     * @param mixed $value
     * @return float
     */
    protected function normalizeValue($value): float
    {
        return $value + 0.0;
    }
}