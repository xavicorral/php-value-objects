<?php

namespace ValueObjects\Geography;

use Exception;
use Symfony\Component\Intl\Languages;
use ValueObjects\AbstractValueObject;
use ValueObjects\Exception\Geography\InvalidLanguageCodeException;

class LanguageCode extends AbstractValueObject
{
    /**
     * Guard that value object is valid.
     *
     * @param string $value
     *
     * @return boolean
     * @throws InvalidLanguageCodeException
     */
    protected function guard($value)
    {
        try {
            Languages::getName($value);
        } catch(Exception $e) {
            throw new InvalidLanguageCodeException($value);
        }

        return true;
    }
}