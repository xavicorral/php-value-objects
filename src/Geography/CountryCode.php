<?php

namespace ValueObjects\Geography;

use Exception;
use Symfony\Component\Intl\Countries;
use ValueObjects\AbstractValueObject;
use ValueObjects\Exception\Geography\InvalidCountryCodeException;

class CountryCode extends AbstractValueObject
{
    /**
     * Guard that value object is valid.
     *
     * @param string $value
     *
     * @return boolean
     * @throws InvalidCountryCodeException
     */
    protected function guard($value)
    {
        $value = $this->normalizeValue($value);
        try {
            Countries::getName($value);
        } catch (Exception $e) {
            throw new InvalidCountryCodeException($value);
        }

        return true;
    }

    protected function normalizeValue($value): string
    {
        return strtoupper($value);
    }
}