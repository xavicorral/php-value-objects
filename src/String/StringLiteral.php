<?php

namespace ValueObjects\String;

use ValueObjects\AbstractValueObject;
use ValueObjects\Exception\String\InvalidStringException;

class StringLiteral extends AbstractValueObject
{
    /**
     * Guard that value object is valid.
     *
     * @param string $value
     *
     * @return boolean
     * @throws InvalidStringException
     */
    protected function guard($value)
    {
        if (false === is_string($value)){
            throw new InvalidStringException($value);
        }

        return true;
    }
}