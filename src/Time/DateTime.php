<?php

namespace ValueObjects\Time;

use DateTimeImmutable;
use DateTimeZone;
use ValueObjects\AbstractValueObject;
use ValueObjects\Exception\Time\InvalidDateTimeException;

class DateTime extends AbstractValueObject
{
    const DEFAULT_FORMAT = 'Y-m-d H:i:s';
    const DEFAULT_TIMEZONE = 'UTC';

    private $format;
    private $timeZone;
    private $datetime;
    private $year;
    private $month;
    private $day;
    private $hour;
    private $minute;
    private $second;

    public function __construct(?string $value, string $format = self::DEFAULT_FORMAT, $timeZone = self::DEFAULT_TIMEZONE)
    {
        $this->format = $format;
        $this->timeZone = $timeZone;
        parent::__construct($value);
    }

    /**
     * Guard that value object is valid.
     *
     * @param string $value
     *
     * @return boolean
     * @throws InvalidDateTimeException
     */
    protected function guard($value): bool
    {
        $datetime = date_parse_from_format($this->format, $value);
        if (!empty($datetime['warning_count']) || !empty($datetime['error_count'])) {
            throw new InvalidDateTimeException($value, $this->format);
        }

        $this->year = new Year($datetime['year']);
        $this->month = new Month($datetime['month']);
        $this->day = new Day($datetime['day']);
        $this->hour = new Hour($datetime['hour']);
        $this->minute = new Minute($datetime['minute']);
        $this->second = new Second($datetime['second']);
        $this->datetime = DateTimeImmutable::createFromFormat($this->format, $this->normalizeValue($value), new DateTimeZone($this->timeZone));

        if (false === $this->datetime) {
            throw new InvalidDateTimeException($value, $this->format);
        }

        return true;
    }

    public function format(): string
    {
        return $this->format;
    }

    public function datetime(): DateTimeImmutable
    {
        return $this->datetime;
    }

    public function getYear(): Year
    {
        return clone $this->year;
    }

    public function getMonth(): Month
    {
        return clone $this->month;
    }

    public function getDay(): Day
    {
        return clone $this->day;
    }

    public function getHour(): Hour
    {
        return clone $this->hour;
    }

    public function getMinute(): Minute
    {
        return clone $this->minute;
    }

    public function getSecond(): Second
    {
        return clone $this->second;
    }

    public static function now(string $format = self::DEFAULT_FORMAT, $timeZone = self::DEFAULT_TIMEZONE): DateTime
    {
        return new static(date($format), $format, $timeZone);
    }

    public function equalsTo($otherDate): bool
    {
        /** @var self $otherDate */
        $datetimeOne = \DateTime::createFromFormat($this->format, $this->value);
        $datetimeTwo = \DateTime::createFromFormat($otherDate->format(), $otherDate->value());

        return $datetimeOne == $datetimeTwo;
    }

    public function greaterThan($otherDate): bool
    {
        $datetimeOne = \DateTime::createFromFormat($this->format, $this->value);
        $datetimeTwo = \DateTime::createFromFormat($otherDate->format(), $otherDate->value());

        return $datetimeOne > $datetimeTwo;
    }

    public function greaterThanEquals($otherDate): bool
    {
        $datetimeOne = \DateTime::createFromFormat($this->format, $this->value);
        $datetimeTwo = \DateTime::createFromFormat($otherDate->format(), $otherDate->value());

        return $datetimeOne >= $datetimeTwo;
    }

    public function lowerThan($otherDate): bool
    {
        $datetimeOne = \DateTime::createFromFormat($this->format, $this->value);
        $datetimeTwo = \DateTime::createFromFormat($otherDate->format(), $otherDate->value());

        return $datetimeOne < $datetimeTwo;
    }

    public function lowerThanEquals($otherDate): bool
    {
        $datetimeOne = \DateTime::createFromFormat($this->format, $this->value);
        $datetimeTwo = \DateTime::createFromFormat($otherDate->format(), $otherDate->value());

        return $datetimeOne <= $datetimeTwo;
    }

    /**
     * Normalize the value.
     *
     * @param mixed $value
     * @return mixed
     */
    protected function normalizeValue($value)
    {
        return ''.date($this->format, mktime(
            $this->hour->value(),
            $this->minute->value(),
            $this->second->value(),
            $this->month->value(),
            $this->day->value(),
            $this->year->value()
        ));
    }
}