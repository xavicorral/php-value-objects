<?php

namespace ValueObjects\Time;

use ValueObjects\AbstractValueObject;
use ValueObjects\Exception\Time\InvalidDayException;

class Day extends AbstractValueObject
{
    private const MIN_DAY = 1;
    private const MAX_DAY = 31;

    /**
     * Guard that value object is valid.
     *
     * @param string $value
     *
     * @return boolean
     * @throws InvalidDayException
     */
    protected function guard($value)
    {
        if ($value < self::MIN_DAY || $value > self::MAX_DAY) {
            throw new InvalidDayException($value);
        }

        return true;
    }
}