<?php

namespace ValueObjects\Time;

use ValueObjects\AbstractValueObject;
use ValueObjects\Exception\Time\InvalidTimeException;

class Time extends AbstractValueObject
{
    const DEFAULT_FORMAT = 'H:i:s';

    private $format;
    private $hour;
    private $minute;
    private $second;

    public function __construct(?string $value, string $format = self::DEFAULT_FORMAT)
    {
        $this->format = $format;
        parent::__construct($value);
    }

    /**
     * Guard that value object is valid.
     *
     * @param string $value
     *
     * @return boolean
     * @throws InvalidTimeException
     */
    protected function guard($value)
    {
        $time = date_parse_from_format($this->format, $value);
        if (!empty($time['warning_count']) || !empty($time['error_count'])) {
            throw new InvalidTimeException($value);
        }

        $this->hour = new Hour($time['hour']);
        $this->minute = new Minute($time['minute']);
        $this->second = new Second($time['second']);

        return true;
    }

    public function getHour(): Hour
    {
        return clone $this->hour;
    }

    public function getMinute(): Minute
    {
        return clone $this->minute;
    }

    public function getSecond(): Second
    {
        return clone $this->second;
    }

    public static function now($format = self::DEFAULT_FORMAT): Time
    {
        return new static(date($format), $format);
    }

    /**
     * Normalize the value.
     *
     * @param mixed $value
     * @return mixed
     */
    protected function normalizeValue($value)
    {
        return ''.date($this->format, mktime($this->hour->value(), $this->minute->value(), $this->second->value()));
    }
}