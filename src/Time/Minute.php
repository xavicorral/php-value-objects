<?php

namespace ValueObjects\Time;

use ValueObjects\AbstractValueObject;
use ValueObjects\Exception\Time\InvalidMinuteException;

class Minute extends AbstractValueObject
{
    private const MIN_MINUTE = 0;
    private const MAX_MINUTE = 59;

    /**
     * Guard that value object is valid.
     *
     * @param string $value
     *
     * @return boolean
     * @throws InvalidMinuteException
     */
    protected function guard($value)
    {
        $filteredValue = filter_var($value, FILTER_VALIDATE_INT);

        // FILTER_VALIDATE_INT validates true as 1.
        if (true === $value || (false === $filteredValue && !is_numeric($value)) || $value < self::MIN_MINUTE || $value > self::MAX_MINUTE) {
            throw new InvalidMinuteException($value);
        }

        return true;
    }
}