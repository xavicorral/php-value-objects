<?php

namespace ValueObjects\Time;

use InvalidArgumentException;
use ValueObjects\Exception\Time\InvalidYearException;
use ValueObjects\Number\Natural;

class Year extends Natural
{
    /**
     * Guard that value object is valid.
     *
     * @param string $value
     *
     * @return boolean
     * @throws InvalidYearException
     */
    protected function guard($value)
    {
        try {
            parent::guard($value);
        } catch(InvalidArgumentException $e) {
            throw new InvalidYearException($value);
        }

        return true;
    }
}