<?php

namespace ValueObjects\Time;

use ValueObjects\AbstractValueObject;
use ValueObjects\Exception\Time\InvalidMonthException;

class Month extends AbstractValueObject
{
    private const MIN_MONTH = 1;
    private const MAX_MONTH = 12;

    /**
     * Guard that value object is valid.
     *
     * @param string $value
     *
     * @return boolean
     * @throws InvalidMonthException
     */
    protected function guard($value)
    {
        if ($value < self::MIN_MONTH || $value > self::MAX_MONTH) {
            throw new InvalidMonthException($value);
        }

        return true;
    }
}