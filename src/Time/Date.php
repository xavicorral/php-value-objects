<?php

namespace ValueObjects\Time;

use ValueObjects\AbstractValueObject;
use ValueObjects\Exception\Time\InvalidDateException;

class Date extends AbstractValueObject
{
    private $format;
    private $year;
    private $month;
    private $day;

    public function __construct(?string $value, string $format = 'Y-m-d')
    {
        $this->format = $format;
        parent::__construct($value);
    }

    /**
     * Guard that value object is valid.
     *
     * @param string $value
     *
     * @return boolean
     * @throws InvalidDateException
     */
    protected function guard($value)
    {
        $date = date_parse_from_format($this->format, $value);
        if (!empty($date['warning_count']) || !empty($date['error_count'])) {
            throw new InvalidDateException($value);
        }

        $this->year = new Year($date['year']);
        $this->month = new Month($date['month']);
        $this->day = new Day($date['day']);

        return true;
    }

    public function format(): string
    {
        return $this->format;
    }

    public function getYear(): Year
    {
        return clone $this->year;
    }

    public function getMonth(): Month
    {
        return clone $this->month;
    }

    public function getDay(): Day
    {
        return clone $this->day;
    }

    public function equalsTo($otherDate): bool
    {
        /** @var self $otherDate */
        $datetimeOne = \DateTime::createFromFormat($this->format, $this->value);
        $datetimeTwo = \DateTime::createFromFormat($otherDate->format(), $otherDate->value());

        return $datetimeOne == $datetimeTwo;
    }

    public function greaterThan($otherDate): bool
    {
        $datetimeOne = \DateTime::createFromFormat($this->format, $this->value);
        $datetimeTwo = \DateTime::createFromFormat($otherDate->format(), $otherDate->value());

        return $datetimeOne > $datetimeTwo;
    }

    public function greaterThanEquals($otherDate): bool
    {
        $datetimeOne = \DateTime::createFromFormat($this->format, $this->value);
        $datetimeTwo = \DateTime::createFromFormat($otherDate->format(), $otherDate->value());

        return $datetimeOne >= $datetimeTwo;
    }

    public function lowerThan($otherDate): bool
    {
        $datetimeOne = \DateTime::createFromFormat($this->format, $this->value);
        $datetimeTwo = \DateTime::createFromFormat($otherDate->format(), $otherDate->value());

        return $datetimeOne < $datetimeTwo;
    }

    public function lowerThanEquals($otherDate): bool
    {
        $datetimeOne = \DateTime::createFromFormat($this->format, $this->value);
        $datetimeTwo = \DateTime::createFromFormat($otherDate->format(), $otherDate->value());

        return $datetimeOne <= $datetimeTwo;
    }

    public function max(self $otherDate): self
    {
        if ($otherDate->greaterThanEquals($this)) {
            return $otherDate;
        }

        return $this;
    }

    public function min(self $otherDate): self
    {
        if ($otherDate->lowerThanEquals($this)) {
            return $otherDate;
        }

        return $this;
    }

    public function add(string $intervalExpression)
    {
        $datetime = \DateTime::createFromFormat($this->format, $this->value);
        $datetime->add(new \DateInterval($intervalExpression));

        return new self($datetime->format('Y-m-d'));
    }

    public static function now(string $format = 'Y-m-d'): Date
    {
        return new static(date($format), $format);
    }

    /**
     * Normalize the value.
     *
     * @param mixed $value
     * @return mixed
     */
    protected function normalizeValue($value)
    {
        return ''.date($this->format, mktime(0, 0, 0, $this->month->value(), $this->day->value(), $this->year->value()));
    }
}