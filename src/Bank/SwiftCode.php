<?php

declare(strict_types=1);

namespace ValueObjects\Bank;

use ValueObjects\Exception\Bank\InvalidSwiftCodeException;
use ValueObjects\String\StringLiteral;

class SwiftCode extends StringLiteral
{
    protected function guard($value)
    {
        $value = $this->normalizeValue($value);
        if (!preg_match('#^[A-Z]{4}[A-Z]{2}[0-9A-Z]{2}[0-9A-Z]{3}$#', $value)) {
            throw new InvalidSwiftCodeException($value);
        }

        return true;
    }

    protected function normalizeValue($value)
    {
        return strtoupper($value);
    }
}
