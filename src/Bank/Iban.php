<?php

declare(strict_types=1);

namespace ValueObjects\Bank;

use ValueObjects\Exception\Bank\InvalidIbanException;
use ValueObjects\String\StringLiteral;

class Iban extends StringLiteral
{
    protected function guard($value)
    {
        $value = $this->normalizeValue($value);

        $validator = new \CMPayments\IBAN($value);
        if (!$validator->validate()) {
            throw new InvalidIbanException($value);
        }

        return true;
    }

    protected function normalizeValue($value)
    {
        return preg_replace("/[^A-Z0-9]/", '', strtoupper($value));
    }
}
