<?php

namespace ValueObjects\Number;

use ValueObjects\AbstractValueObject;
use ValueObjects\Exception\Number\InvalidIntegerException;

class Integer extends AbstractValueObject
{
    /**
     * Guard that value object is valid.
     *
     * @param integer $value
     *
     * @return boolean
     * @throws InvalidIntegerException
     */
    protected function guard($value)
    {
        $filteredValue = filter_var($value, FILTER_VALIDATE_INT);

        // FILTER_VALIDATE_INT validates true as 1.
        if (true === $value || false === $filteredValue){
            throw new InvalidIntegerException($value);
        }

        return true;
    }

    /**
     * Convert the valid integer (string, int...) to native integer.
     *
     * @param mixed $value
     * @return int
     */
    protected function normalizeValue($value): int
    {
        return $value + 0;
    }
}