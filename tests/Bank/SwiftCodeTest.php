<?php

namespace ValueObjects\Tests\Bank;

use ValueObjects\Exception\Bank\InvalidSwiftCodeException;
use ValueObjects\Bank\SwiftCode;

class SwiftCodeTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider validValuesProvider
     */
    public function testValidValues($value)
    {
        $valueObject = new SwiftCode($value);
        $this->assertSame(strtoupper($value), $valueObject->value());
    }

    public function validValuesProvider()
    {
        return array(
            'Spanish SWIFT code' => ['GALEES2GXXX'],
            'Spanish SWIFT code lowercase' => ['bapues22xxx'],
            'German SWIFT code' => ['AARBDE5WDOM'],
            'Italian SWIFT code' => ['CCRTIT2TA01'],
            'French SWIFT code' => ['AIGAFRPPXXX'],
        );
    }

    /**
     * @dataProvider notValidValuesProvider
     */
    public function testNotValidValues($value)
    {
        $this->setExpectedException(InvalidSwiftCodeException::class);
        new SwiftCode($value);
    }

    public function notValidValuesProvider()
    {
        return array(
            'Less chars' => ['GALES2GXXX'],
            'More chars' => ['AARBDE5WDOM3'],
            'Start with numbers' => ['G3LEES2GXXX'],
            'Invalid chars' => ['GALE-S2GXXX'],
        );
    }
}