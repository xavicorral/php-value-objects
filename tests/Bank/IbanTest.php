<?php

namespace ValueObjects\Tests\Bank;

use ValueObjects\Exception\Bank\InvalidIbanException;
use ValueObjects\Bank\Iban;

class IbanTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider validValuesProvider
     */
    public function testValidValues($value, $expected)
    {
        $valueObject = new Iban($value);
        $this->assertSame($expected, $valueObject->value());
    }

    public function validValuesProvider()
    {
        return array(
            'Spanish IBAN' => ['ES5301284454845659267339', 'ES5301284454845659267339'],
            'Spanish IBAN lowercase' => ['es5301284454845659267339', 'ES5301284454845659267339'],
            'Spanish IBAN with white spaces' => ['ES53 0128 4454 8456 5926 7339', 'ES5301284454845659267339'],
            'Spanish IBAN with underscore chars' => ['ES53_0128_4454_8456_5926_7339', 'ES5301284454845659267339'],
            'Spanish IBAN with dash chars' => ['ES53-0128-4454-8456-5926-7339', 'ES5301284454845659267339'],
            'German IBAN' => ['DE50500105174675635587', 'DE50500105174675635587'],
            'Italian IBAN' => ['IT42I0300203280148932564199', 'IT42I0300203280148932564199'],
            'French IBAN' => ['FR4312739000308688273333W54', 'FR4312739000308688273333W54'],
            'Netherlands IBAN' => ['NL49INGB6496646791', 'NL49INGB6496646791'],
            'Belgium IBAN' => ['BE88173648133441', 'BE88173648133441'],
        );
    }

    /**
     * @dataProvider notValidValuesProvider
     */
    public function testNotValidValues($value)
    {
        $this->setExpectedException(InvalidIbanException::class);
        new Iban($value);
    }

    public function notValidValuesProvider()
    {
        return array(
            'Less chars for Spanish IBAN' => ['ES5301284454845659267'],
            'More chars for Spanish IBAN' => ['ES530128445484565926733925'],
            'Start with numbers' => ['53ES01284454845659267339'],
            'Invalid checksum' => ['ES5301284354845659267339'],
        );
    }
}