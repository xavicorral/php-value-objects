<?php

namespace ValueObjects\Tests\Boolean;

use ValueObjects\Exception\Boolean\InvalidBooleanTypeException;
use ValueObjects\Boolean\BooleanType;

class BooleanTypeTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider validValuesProvider
     */
    public function testValidValues($value)
    {
        $valueObject = new BooleanType($value);
        $this->assertSame($value, $valueObject->value());
    }

    public function validValuesProvider()
    {
        return array(
            'true is valid' => [true],
            'false is valid' => [true],
        );
    }
    /**
     * @dataProvider validFromStringValuesProvider
     */
    public function testValidFromStringValues($value, bool $expected)
    {
        $valueObject = BooleanType::fromString($value);
        $this->assertSame($expected, $valueObject->value());
    }

    public function validFromStringValuesProvider()
    {
        return array(
            'String "true" is not valid' => ['true', true],
            'String "false" is not valid' => ['false', false],
            'String "yes" is not valid' => ['yes', true],
            'String "no" is not valid' => ['no', false],
            'String "1" is not valid' => ['1', true],
            'String "0" is not valid' => ['0', false],
            'Int "1" is not valid' => [1, true],
            'Int "0" is not valid' => [0, false],
        );
    }

    /**
     * @dataProvider notValidValuesProvider
     */
    public function testNotValidValues($value)
    {
        $this->setExpectedException(InvalidBooleanTypeException::class);
        new BooleanType($value);
    }

    public function notValidValuesProvider()
    {
        return array(
            'String "true" is not valid' => ['true'],
            'String "false" is not valid' => ['false'],
            'String "yes" is not valid' => ['yes'],
            'String "no" is not valid' => ['no'],
            'Int "1" is not valid' => [1],
            'Int "0" is not valid' => [0],
        );
    }

    /**
     * @dataProvider toIntConversionProvider
     */
    public function testToIntConversion($value, $expected)
    {
        $valueObject = new BooleanType($value);

        $this->assertSame($expected, $valueObject->toInt());
    }

    public function toIntConversionProvider()
    {
        return array(
            'Convert true to 1' => [true, 1],
            'Convert false to 0' => [false, 0],
        );
    }

    /**
     * @dataProvider toStringConversionProvider
     */
    public function testToStringConversion($value, $expected)
    {
        $valueObject = new BooleanType($value);

        $this->assertSame($expected, $valueObject->toString());
        $this->assertSame($expected, (string) $valueObject);
    }

    public function toStringConversionProvider()
    {
        return array(
            'Convert true to "true"' => [true, 'true'],
            'Convert false to "false"' => [false, 'false'],
        );
    }
}