<?php

namespace ValueObjects\Tests\Time;

use ValueObjects\Exception\Time\InvalidDateTimeException;
use ValueObjects\Time\DateTime;

class DateTimeTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider validValuesProvider
     */
    public function testValidValues($value, $format, $expected)
    {
        $valueObject = new DateTime($value, $format);
        $this->assertSame($expected, $valueObject->value());
    }

    public function validValuesProvider()
    {
        return array(
            'DateTime "2007-12-31 23:59:59" is valid' => ['2007-12-31 23:59:59', 'Y-m-d H:i:s', '2007-12-31 23:59:59'],
            'DateTime "2007-2-1 01:02:03" is valid and normalized according with format' => ['2007-2-1 01:02:03', 'Y-m-d H:i:s', '2007-02-01 01:02:03'],
            'DateTime for leap-year is valid' => ['2020-02-29 00:00:00', 'Y-m-d H:i:s', '2020-02-29 00:00:00'],
            'DateTime "2007-31-12 03:30:28" is valid for format "Y-d-m H:i:s"' => ['2007-31-12 03:30:28', 'Y-d-m H:i:s', '2007-31-12 03:30:28']
        );
    }

    public function testDefaultFormat()
    {
        $value = '2012-12-31 12:25:32';
        $valueObject = new DateTime($value);
        $this->assertSame($value, $valueObject->value());
    }

    public function testNowDateTime()
    {
        $format = 'Y_d_m H-i-s';
        $now = date($format);
        $this->assertSame($now, DateTime::now($format)->value());
    }

    /**
     * @dataProvider notValidValuesProvider
     */
    public function testNotValidValues($value, $format)
    {
        $this->setExpectedException(InvalidDateTimeException::class);
        new DateTime($value, $format);
    }

    public function notValidValuesProvider()
    {
        return array(
            'DateTime with day out for range is not valid' => ['2007-11-31 00:00:00', 'Y-m-d H:i:s'],
            'DateTime with month out for range is not valid' => ['2007-13-31 00:00:00', 'Y-m-d H:i:s'],
            'DateTime with hour out for range is not valid' => ['2007-13-31 24:00:00', 'Y-m-d H:i:s'],
            'DateTime with minute out for range is not valid' => ['2007-13-31 00:60:00', 'Y-m-d H:i:s'],
            'DateTime with second out for range is not valid' => ['2007-13-31 00:00:60', 'Y-m-d H:i:s'],
            'DateTime not according with format is not valid' => ['2007_12_31 00:00:00', 'Y-m-d H_i_s'],
            '29 of febrary for not leap-year is not valid' => ['2019-02-29 00:00:00', 'Y-m-d H:i:s']
        );
    }

    /**
     * @dataProvider equalsToProvider
     */
    public function testEqualsTo($dateOne, $formatOne, $dateTwo, $formatTwo, $expected): void
    {
        $valueObjectOne = new DateTime($dateOne, $formatOne);
        $valueObjectTwo = new DateTime($dateTwo, $formatTwo);

        $this->assertEquals($expected, $valueObjectOne->equalsTo($valueObjectTwo));
    }

    public function equalsToProvider(): array
    {
        return [
            'Equal date with same format'         => ['2012-12-01 00:00:00', 'Y-m-d H:i:s', '2012-12-01 00:00:00', 'Y-m-d H:i:s', true],
            'Not qual date with same format'      => ['2013-12-01 00:00:00', 'Y-m-d H:i:s', '2012-12-01 00:00:00', 'Y-m-d H:i:s', false],
            'Equal date with different format'    => ['2012-12-01 00:00:00', 'Y-m-d H:i:s', '01-12-2012 00/00/00', 'd-m-Y H/i/s', true],
            'Not qual date with different format' => ['2013-01-01 00:00:00', 'Y-m-d H:i:s', '01-12-2012 00/00/00', 'd-m-Y H/i/s', false],
        ];
    }

    /**
     * @dataProvider greaterThanProvider
     */
    public function testGreaterThan($dateOne, $formatOne, $dateTwo, $formatTwo, $expected): void
    {
        $valueObjectOne = new DateTime($dateOne, $formatOne);
        $valueObjectTwo = new DateTime($dateTwo, $formatTwo);

        $this->assertEquals($expected, $valueObjectOne->greaterThan($valueObjectTwo));
    }

    public function greaterThanProvider(): array
    {
        return [
            'Greater than date with same format'          => ['2013-12-01 00:00:00', 'Y-m-d H:i:s', '2012-12-01 00:00:00', 'Y-m-d H:i:s', true],
            'Equals than date with same format'           => ['2012-12-01 00:00:00', 'Y-m-d H:i:s', '2012-12-01 00:00:00', 'Y-m-d H:i:s', false],
            'Not greater than date with same format'      => ['2012-12-01 00:00:00', 'Y-m-d H:i:s', '2013-12-01 00:00:00', 'Y-m-d H:i:s', false],
            'Greater than date with different format'     => ['2013-12-01 00:00:00', 'Y-m-d H:i:s', '01-12-2012 00/00/00', 'd-m-Y H/i/s', true],
            'Equals than date with different format'      => ['2012-12-01 00:00:00', 'Y-m-d H:i:s', '01-12-2012 00/00/00', 'd-m-Y H/i/s', false],
            'Not greater than date with different format' => ['2012-12-01 00:00:00', 'Y-m-d H:i:s', '01-12-2013 00/00/00', 'd-m-Y H/i/s', false],
            'Greater than time with same format'          => ['2012-12-01 23:01:02', 'Y-m-d H:i:s', '01-12-2012 23:01:00', 'd-m-Y H:i:s', true],
            'Greater than time with different format'     => ['2012-12-01 23:01:02', 'Y-m-d H:i:s', '01-12-2012 23/01/00', 'd-m-Y H/i/s', true],
            'Not greater than time with same format'      => ['2012-12-01 23:01:00', 'Y-m-d H:i:s', '01-12-2012 23:01:02', 'd-m-Y H:i:s', false],
            'Not greater than time with different format' => ['2012-12-01 23:01:00', 'Y-m-d H:i:s', '01-12-2012 23/01/02', 'd-m-Y H/i/s', false],
        ];
    }

    /**
     * @dataProvider greaterThanEqualsProvider
     */
    public function testGreaterThanEquals($dateOne, $formatOne, $dateTwo, $formatTwo, $expected): void
    {
        $valueObjectOne = new DateTime($dateOne, $formatOne);
        $valueObjectTwo = new DateTime($dateTwo, $formatTwo);

        $this->assertEquals($expected, $valueObjectOne->greaterThanEquals($valueObjectTwo));
    }

    public function greaterThanEqualsProvider(): array
    {
        return [
            'Greater than date with same format'          => ['2013-12-01 00:00:00', 'Y-m-d H:i:s', '2012-12-01 00:00:00', 'Y-m-d H:i:s', true],
            'Equals than date with same format'           => ['2012-12-01 00:00:00', 'Y-m-d H:i:s', '2012-12-01 00:00:00', 'Y-m-d H:i:s', true],
            'Not greater than date with same format'      => ['2012-12-01 00:00:00', 'Y-m-d H:i:s', '2013-12-01 00:00:00', 'Y-m-d H:i:s', false],
            'Greater than date with different format'     => ['2013-12-01 00:00:00', 'Y-m-d H:i:s', '01-12-2012 00/00/00', 'd-m-Y H/i/s', true],
            'Equals than date with different format'      => ['2012-12-01 00:00:00', 'Y-m-d H:i:s', '01-12-2012 00/00/00', 'd-m-Y H/i/s', true],
            'Not greater than date with different format' => ['2012-12-01 00:00:00', 'Y-m-d H:i:s', '01-12-2013 00/00/00', 'd-m-Y H/i/s', false],
            'Greater than time with same format'          => ['2012-12-01 23:01:02', 'Y-m-d H:i:s', '2012-12-01 23:01:00', 'Y-m-d H:i:s', true],
            'Greater than time with different format'     => ['2012-12-01 23:01:02', 'Y-m-d H:i:s', '01-12-2012 23/01/00', 'd-m-Y H/i/s', true],
            'Not greater than time with same format'      => ['2012-12-01 23:01:00', 'Y-m-d H:i:s', '2012-12-01 23:01:02', 'Y-m-d H:i:s', false],
            'Not greater than time with different format' => ['2012-12-01 23:01:00', 'Y-m-d H:i:s', '01-12-2012 23/01/02', 'd-m-Y H/i/s', false],
        ];
    }

    /**
     * @dataProvider lowerThanProvider
     */
    public function testLowerThan($dateOne, $formatOne, $dateTwo, $formatTwo, $expected): void
    {
        $valueObjectOne = new DateTime($dateOne, $formatOne);
        $valueObjectTwo = new DateTime($dateTwo, $formatTwo);

        $this->assertEquals($expected, $valueObjectOne->lowerThan($valueObjectTwo));
    }

    public function lowerThanProvider(): array
    {
        return [
            'Lower than date with same format'          => ['2012-12-01 00:00:00', 'Y-m-d H:i:s', '2013-12-01 00:00:00', 'Y-m-d H:i:s', true],
            'Equals than date with same format'         => ['2012-12-01 00:00:00', 'Y-m-d H:i:s', '2012-12-01 00:00:00', 'Y-m-d H:i:s', false],
            'Not lower than date with same format'      => ['2013-12-01 00:00:00', 'Y-m-d H:i:s', '2012-12-01 00:00:00', 'Y-m-d H:i:s', false],
            'Lower than date with different format'     => ['01-12-2012 00/00/00', 'd-m-Y H/i/s', '2013-12-01 00:00:00', 'Y-m-d H:i:s', true],
            'Equals than date with different format'    => ['2012-12-01 00:00:00', 'Y-m-d H:i:s', '01-12-2012 00/00/00', 'd-m-Y H/i/s', false],
            'Not lower than date with different format' => ['01-12-2013 00/00/00', 'd-m-Y H/i/s', '2012-12-01 00:00:00', 'Y-m-d H:i:s', false],
            'Lower than time with same format'          => ['2012-12-01 23:01:00', 'Y-m-d H:i:s', '2012-12-01 23:01:02', 'Y-m-d H:i:s', true],
            'Lower than time with different format'     => ['2012-12-01 23:01:00', 'Y-m-d H:i:s', '01-12-2012 23/01/02', 'd-m-Y H/i/s', true],
            'Not lower than time with same format'      => ['2012-12-01 23:01:02', 'Y-m-d H:i:s', '2012-12-01 23:01:00', 'Y-m-d H:i:s', false],
            'Not lower than time with different format' => ['2012-12-01 23:01:02', 'Y-m-d H:i:s', '01-12-2012 23/01/00', 'd-m-Y H/i/s', false],
        ];
    }

    /**
     * @dataProvider lowerThanEqualsProvider
     */
    public function testLowerThanEquals($dateOne, $formatOne, $dateTwo, $formatTwo, $expected): void
    {
        $valueObjectOne = new DateTime($dateOne, $formatOne);
        $valueObjectTwo = new DateTime($dateTwo, $formatTwo);

        $this->assertEquals($expected, $valueObjectOne->lowerThanEquals($valueObjectTwo));
    }

    public function lowerThanEqualsProvider(): array
    {
        return [
            'Lower than date with same format'          => ['2012-12-01 00:00:00', 'Y-m-d H:i:s', '2013-12-01 00:00:00', 'Y-m-d H:i:s', true],
            'Equals than date with same format'         => ['2012-12-01 00:00:00', 'Y-m-d H:i:s', '2012-12-01 00:00:00', 'Y-m-d H:i:s', true],
            'Not lower than date with same format'      => ['2013-12-01 00:00:00', 'Y-m-d H:i:s', '2012-12-01 00:00:00', 'Y-m-d H:i:s', false],
            'Lower than date with different format'     => ['01-12-2012 00/00/00', 'd-m-Y H/i/s', '2013-12-01 00:00:00', 'Y-m-d H:i:s', true],
            'Equals than date with different format'    => ['2012-12-01 00:00:00', 'Y-m-d H:i:s', '01-12-2012 00/00/00', 'd-m-Y H/i/s', true],
            'Not lower than date with different format' => ['01-12-2013 00/00/00', 'd-m-Y H/i/s', '2012-12-01 00:00:00', 'Y-m-d H:i:s', false],
            'Lower than time with same format'          => ['2012-12-01 23:01:00', 'Y-m-d H:i:s', '2012-12-01 23:01:02', 'Y-m-d H:i:s', true],
            'Lower than time with different format'     => ['2012-12-01 23:01:00', 'Y-m-d H:i:s', '01-12-2012 23/01/02', 'd-m-Y H/i/s', true],
            'Not lower than time with same format'      => ['2012-12-01 23:01:02', 'Y-m-d H:i:s', '2012-12-01 23:01:00', 'Y-m-d H:i:s', false],
            'Not lower than time with different format' => ['2012-12-01 23:01:02', 'Y-m-d H:i:s', '01-12-2012 23/01/00', 'd-m-Y H/i/s', false],
        ];
    }
}